﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Task1_Spelling
{
    class Program
    {
        static void Main(string[] args)
        {
            int case_count = Enter_count_cases();
            List<string> list_lines = Filling_list(case_count);
            Compute(case_count, list_lines);
            Print_lines(case_count, list_lines);
        }


        static int Enter_count_cases()
        {
            int count;
            while (true)
            {
                Console.Write("Please enter the number of cases : ");
                if (Int32.TryParse(Console.ReadLine(), out count))
                    break;
                else
                {
                    Console.Write("Wrong input! ");
                    continue;
                }
            }
            return count;
        }


        static List<string> Filling_list(int count)
        {
            Regex reg = new Regex(@"[^a-z ]+");
            List<string> list = new List<string>();
            for (int i = 0; i < count; i++)
            {
                while (true)
                {
                    Console.Write("Please enter case #{0}: ", i + 1);
                    string asist = Console.ReadLine();
                    if (reg.IsMatch(asist))
                    {
                        Console.Write("Wrong input! ");
                        continue;
                    }
                    else
                    {
                        list.Add(asist);
                        break;
                    }
                }
            }
            return list;
        }


        static void Compute(int case_count, List<string> list_lines)
        {
            for (int x = 0; x < list_lines.Count; x++)
            {
                string asist = " ";
                string str = "";
                for (int i = 0; i < list_lines[x].Length; i++)
                {
                    str = Dict(Convert.ToString(list_lines[x][i]));
                    if (asist[asist.Length - 1] == str[0])
                        asist += " ";
                    asist += str;
                }
                list_lines[x] = asist;
            }
        }


        static void Print_lines(int case_count, List<string> list_lines)
        {
            for (int i = 0; i < list_lines.Count; i++)
                Console.WriteLine("Case #{0} :{1}", i+1, list_lines[i]);
            Console.ReadKey();
        }
        

        static string Dict(string key)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();
            d.Add("a", "2");
            d.Add("b", "22");
            d.Add("c", "222");
            d.Add("d", "3");
            d.Add("e", "33");
            d.Add("f", "333");
            d.Add("g", "4");
            d.Add("h", "44");
            d.Add("i", "444");
            d.Add("j", "5");
            d.Add("k", "55");
            d.Add("l", "555");
            d.Add("m", "6");
            d.Add("n", "66");
            d.Add("o", "666");
            d.Add("p", "7");
            d.Add("q", "77");
            d.Add("r", "777");
            d.Add("s", "7777");
            d.Add("t", "8");
            d.Add("u", "88");
            d.Add("v", "888");
            d.Add("w", "9");
            d.Add("x", "99");
            d.Add("y", "999");
            d.Add("z", "9999");
            d.Add(" ", "0");
            return d[key];
        }
    }
}
